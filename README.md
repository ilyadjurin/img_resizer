# Image Resizer

Проект развернут на http://resizer.autoton.biz


## Пользователи
Для входа в админ панель используйте логин/пароль:
- admin/testpass


## Развертывание проекта
```
cp .env_default .env && nano .env
docker-compose build
docker-compose up
```

Кэшируемые запросы хранятся сутки.
