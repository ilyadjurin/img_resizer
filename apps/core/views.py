import datetime
import string

import os
import random
import requests
from PIL import Image
from django.http import HttpResponse
from django.shortcuts import redirect
from django.views.generic import TemplateView
from django.views.generic.base import View
from io import BytesIO

from apps.core.models import Picture


class ImageResizeView(TemplateView):
    template_name = 'image.html'

    def get(self, request, *args, **kwargs):
        resize_by_size = False
        context = self.get_context_data(**kwargs)

        if not self.validate_input_data(request.GET):
            context['image_error'] = "Invalid input data"
            return self.render_to_response(context)

        file = Image.open(request.path[1:-1])
        file_size = os.path.getsize(request.path[1:-1])

        if request.GET.get('width'):
            new_img = file.resize([int(request.GET['width']), file.height], Image.LANCZOS)
            img_tmp_name = '{}_{}.{}'.format(kwargs['imgname'][:-4],
                                             datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S_%f"), file.format)
            path = 'static/img_temp/{}'.format(img_tmp_name)
            new_img.save(path, optimize=True, quality=95)
            context['image'] = 'img_temp/{}'.format(img_tmp_name)

        if 'new_img' in locals():
            file = Image.open(path)

        if request.GET.get('height'):
            new_img = file.resize([file.width, int(request.GET['height'])], Image.LANCZOS)
            img_tmp_name = '{}_{}.{}'.format(kwargs['imgname'][:-4],
                                             datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S_%f"), file.format)
            path = 'static/img_temp/{}'.format(img_tmp_name)
            new_img.save(path, optimize=True, quality=95)
            context['image'] = 'img_temp/{}'.format(img_tmp_name)

        if 'new_img' in locals():
            file_size = os.path.getsize(path)
            resize_img = Image.open(path)
        else:
            resize_img = file

        if request.GET.get('size'):
            quality = 95
            while file_size > int(request.GET.get('size')):

                resize_img = resize_img.resize([int(resize_img.width * 0.85), int(resize_img.height * 0.85)],
                                               Image.LANCZOS)
                img_path_size = 'static/img_temp/temp_img.{}'.format(file.format)
                resize_img.save(img_path_size, optimize=True, quality=quality)
                if quality > 10:
                    quality -= 10
                file_size = os.path.getsize(img_path_size)
                resize_by_size = True
            else:
                if int(request.GET.get('size')) > file_size and not resize_by_size:
                    if not context.get('image'):
                        context['image_error'] = "The specified image size is larger than original"
                        return self.render_to_response(context)
                else:
                    file = Image.open(img_path_size)
                    img_tmp_name = '{}_{}.{}'.format(kwargs['imgname'][:-4],
                                                     datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S_%f"),
                                                     file.format)
                    path = 'static/img_temp/{}'.format(img_tmp_name)
                    file.save(path)
                    context['image'] = 'img_temp/{}'.format(img_tmp_name)
        if not context.get('image'):
            image_data = open(request.path[1:-1], "rb").read()
            return HttpResponse(image_data, content_type="image/{}".format(file.format))

        return self.render_to_response(context)

    @staticmethod
    def validate_input_data(params: dict) -> bool:
        for key in ['width', 'height', 'size']:
            if params.get(key) is not None:
                try:
                    int(params.get(key))
                except Exception as error:
                    return False
        else:
            return True


class UploadView(TemplateView):
    template_name = 'upload.html'


class DownloadView(View):
    @staticmethod
    def id_generator(size=8, chars=string.ascii_uppercase + string.digits) -> str:
        return ''.join(random.choice(chars) for _ in range(size))

    def post(self, request):
        if request.method == 'POST':
            if request.FILES.get('myfile'):
                Picture.objects.create(file=request.FILES['myfile'])

            elif request.POST.get('myurl'):
                myurl = request.POST['myurl']
                response = requests.get(myurl)
                file = Image.open(BytesIO(response.content))
                img_tmp_name = '{}_{}.{}'.format(self.id_generator(),
                                                 datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S_%f"), file.format)
                file.save('media/images/{}'.format(img_tmp_name))
                Picture.objects.create(file='images/{}'.format(img_tmp_name))

        return redirect('/')


class BaseView(TemplateView):
    template_name = 'index.html'

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        context['pictures'] = Picture.objects.all()
        return self.render_to_response(context)
