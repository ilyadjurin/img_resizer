from django.db import models

# Create your models here.


class Picture(models.Model):
    file = models.ImageField(upload_to='images/', verbose_name='image')

    class Meta:
        verbose_name = "picture"
        verbose_name_plural = "pictures"
        ordering = ['id']

    def __str__(self):
        return '%s' % (self.id,)
