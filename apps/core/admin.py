from django.contrib import admin

from apps.core.models import Picture


@admin.register(Picture)
class ImageAdmin(admin.ModelAdmin):
    list_display = ('id', 'file')
