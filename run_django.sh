#!/usr/bin/env bash

VENV=./resizevenv

# Remove lock if it's active for some reason
rm -f python.lock

# Lock until venv installation and migration are completed
lockfile python.lock

# if directory not exists
if [ ! -d "$VENV" ]; then
    # create venv
    virtualenv $VENV
fi

source $VENV/bin/activate

# Read sha1 sum to variable
sha1old="$(head -n 1 $VENV/sha1.sum)"
sha1req="$(sha1sum requirements.txt)"
sha1old=${sha1old:0:40}
sha1req=${sha1req:0:40}

# if file doesn't exists or sha1 sum is different
if [ ! -f $VENV/sha1.sum ] || [[ $sha1old != $sha1req ]]; then
    pip install --upgrade pip;
    pip install -U -r requirements.txt;
    pip install uwsgi;
    echo $sha1req > $VENV/sha1.sum;
fi

FILE=./db.sqlite3
if [ ! -f "$FILE" ]; then
    # create cachetable
    python3 manage.py createcachetable cache_table
fi

rm -rfv static/img_temp/*

python3 manage.py migrate
python3 manage.py collectstatic --no-input


rm -f python.lock

uwsgi --yaml ./uwsgi.yml
