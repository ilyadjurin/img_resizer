from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path
from django.views.decorators.cache import cache_page

from apps.core.views import BaseView, ImageResizeView, UploadView, DownloadView

urlpatterns = [
    path('', BaseView.as_view()),
    path('admin/', admin.site.urls),
    path('upload/', UploadView.as_view()),
    path('download/', DownloadView.as_view()),
    path('media/images/<imgname>/', cache_page(60 * 60 * 24)(ImageResizeView.as_view())),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
